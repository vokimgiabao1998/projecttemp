﻿using MaterialDesignThemes.Wpf;
using NailsApplication.ChildViews;
using NailsApplication.Models;
using NailsApplication.UserControlCustom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NailsApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<SubItemUC> lstmenurRegister = new List<SubItemUC>();
            lstmenurRegister.Add(new SubItemUC("Customer"));
            lstmenurRegister.Add(new SubItemUC("Providers"));
            lstmenurRegister.Add(new SubItemUC("Employees"));
            lstmenurRegister.Add(new SubItemUC("Products"));
            var item6 = new IMenuUC("Register", lstmenurRegister, PackIconKind.Register);

            List<SubItemUC> lstmenurReport = new List<SubItemUC>();
            lstmenurReport.Add(new SubItemUC("Report 1"));
            lstmenurReport.Add(new SubItemUC("Report 2"));
            var item5 = new IMenuUC("Report", lstmenurReport, PackIconKind.Register);

            MenuItemUC menuItem1 = new MenuItemUC(item6);
           
            
            if (!menuItem1.mSubcribie) 
            {
                menuItem1.ClickItem += MenuItemClick;
            }

            Menu.Children.Add(menuItem1);
            //Menu.Children.Add(new MenuItemUC(item5));

        }
        /// <summary>
        /// Sender có dữ liệu từ bên Child 
        /// Đây là 1 trong những cách truyền delegate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void MenuItemClick(object sender, EventArgs e)
        {
            var str1 = sender as string;
            MessageBox.Show(str1);
            ChildConfig chcf = new ChildConfig();
            ChildViews.Children.Add(chcf);
            // duoi nay if else or switch 
        }

        private void BarControlUC_Loaded(object sender, RoutedEventArgs e)
        {
            Menu.Visibility = Visibility.Visible;
        }
    }
}
